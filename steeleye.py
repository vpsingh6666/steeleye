import json
import requests as res
import pandas as pd



class NobelPrizeScrapper:
	def __init__(self, noble_prize_winners_data_url: str, country_data_url: str):
		# Columns Needed
		self.columns_needed = {
			'countries':['name', 'code'],
			'laureates':['prizes', 'gender', 'surname', 'firstname', 'id', 'born', 'bornCountryCode']
		}
		self.requested_columns = ['id', 'org', 'name', 'dob', 'unique_prize_years', \
								'unique_prize_categories', 'gender', 'countries']

		self.noble_prize_winners = pd.DataFrame()
		self.country_details = pd.DataFrame()
		self.transfomed_data = pd.DataFrame()


	def downloadFile(self, url:str, key:str) -> pd.DataFrame:
		# Requesting Data
		response = res.get(url)

		# Url is Not Valid 
		if response.status_code != 200:
			raise AttributeError("{} File Not Found on the URL for - {}".format(key, url))

		# Getting Json Data
		data = json.loads(response.content)

		# Key is not Valid
		if not data.get(key):
			raise KeyError("Key Not Found for - {}".format(key))

		data = data[key]

		# Checking if data exists
		if not len(data):
			raise ValueError("Data is not Present in for - {}".format(key))

		# Transforming to Pandas DataFrame
		data = pd.DataFrame(data)

		# Cheking if required columns exists
		missing_columns = [i for i in list(data.columns) if i not in self.columns_needed[key]]
		# Missing Columns
		if missing_columns:
			KeyError("In - {} dataset columns {} are not Present".format(key, ",".join(missing_columns)))

		return data


	def getTransformedData(self) ->  None:
		try:
			# Adding Organisation Column
			con_org = self.noble_prize_winners['gender'].isin(['org']) # Organisation Condition
			self.noble_prize_winners.loc[con_org, 'org'] = self.noble_prize_winners.loc[con_org, 'firstname']

			# Adding Name Column
			con_name = ~self.noble_prize_winners['surname'].isnull() # Name Condition
			self.noble_prize_winners.loc[con_name, 'name'] = self.noble_prize_winners.loc[con_name, 'firstname'] + \
															self.noble_prize_winners.loc[con_name, 'surname']


			# Adding unique_prize_years Column
			self.noble_prize_winners['unique_prize_years'] = self.noble_prize_winners['prizes'].apply(
				lambda x: ";".join(set([str(i['year']) for i in x if i.get('year')]))
			)

			# Adding unique_prize_categories Column
			self.noble_prize_winners['unique_prize_categories'] = self.noble_prize_winners['prizes'].apply(
				lambda x: ";".join(set([str(i['category']) for i in x if i.get('category')]))
			)

			# Renaming Columns
			self.noble_prize_winners.rename(columns= {'born':'dob'}, inplace=True) # Adding Column Born
			self.country_details.rename(columns= {'name':'countries'}, inplace=True) # Renaming Column Contries

			# Merging Prize Winner and Countries Data
			self.transfomed_data = pd.merge(
				self.noble_prize_winners, 
				self.country_details.groupby('code', as_index=False).first(), # Taking 1st Value for similar country codes
				left_on=['bornCountryCode'], 
				right_on=['code'], 
				how='left', 
				indicator=True
			)

			# Final DataFrame
			self.transfomed_data = self.transfomed_data[self.requested_columns]
		except:
			SystemError("Error occured while Transforming Data")



	def saveData(self, filename:str) -> None:
		try:
			self.transfomed_data.to_csv(filename, index=False)
		except:
			SystemError("Error occured while saving file at - {}", filename)
