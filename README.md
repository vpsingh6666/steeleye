# Steeleye Test



## Script to Transform NobelPrize Data

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Files

- [ ] steeleye.py -> It Contains NobelPrizeScrapper Class
- [ ] main.py -> Runs the tranformation when dataset urls are passed


## Running the ETL
```
<!-- Laureates Url -->
arg1='https://raw.githubusercontent.com/ashwath92/nobel_json/main/laureates.json'
<!-- Countries Url -->
arg2='https://raw.githubusercontent.com/ashwath92/nobel_json/main/countries.json'
<!-- Saving File Name Or Path -->
arg3='noble_prize_winners_data.csv'
<!-- Running Transformation -->
python3 main.py $arg1 $arg2 $arg3

```