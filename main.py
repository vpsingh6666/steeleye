import sys
from steeleye import NobelPrizeScrapper


def main(noble_prize_winners_data_url:str, country_data_url:str, saving_file_name:str):
    prize_winners = NobelPrizeScrapper(noble_prize_winners_data_url, country_data_url)
    # Getting noble_prize_winners data
    prize_winners.noble_prize_winners = prize_winners.downloadFile(url=noble_prize_winners_data_url, key='laureates')
    # Getting Country data
    prize_winners.country_details = prize_winners.downloadFile(url=country_data_url, key='countries')
    # Transforming Data
    prize_winners.getTransformedData()
    # Saving Data
    prize_winners.saveData(filename=saving_file_name)


if __name__=="__main__":
	if not (len(sys.argv) == 4 and sys.argv[1] and sys.argv[2] and sys.argv[3]):
		raise AttributeError('''Please Pass noble_prize_winners_data_url, country_data_url, saving_file name in arguments\n example ---> python3 steeleye.py 'https://raw.githubusercontent.com/ashwath92/nobel_json/main/laureates.json' ' https://raw.githubusercontent.com/ashwath92/nobel_json/main/countries.json' 'noble_prize_winners_data.csv'
			''')
	main(
		noble_prize_winners_data_url = sys.argv[1],
		country_data_url = sys.argv[2],
		saving_file_name = sys.argv[3]
	)